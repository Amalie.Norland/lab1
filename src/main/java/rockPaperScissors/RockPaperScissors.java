package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class RockPaperScissors {
	
	public static void main(String[] args) {
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    

    public String getComputerMove() {

        String computerMove = "default";
        Random generator = new Random();
        int choiceInt = generator.nextInt(3);

        if (choiceInt == 0){
            computerMove = rpsChoices.get(0);
        }
        else if (choiceInt == 1){
            computerMove = rpsChoices.get(1);
        }
        else if (choiceInt == 2){
            computerMove = rpsChoices.get(2);
        }
        return computerMove;
    }


    public String getPlayerMove() {
            while(true) {      
                System.out.println("Your choice (Rock/Paper/Scissors)?");  
                String choice = sc.next();
                String playerMove = choice.toLowerCase();
                if (playerMove.equals("rock") || playerMove.equals("paper") || playerMove.equals("scissors")) {
                    return playerMove;
                }
                else {
                    System.out.println("I do not understand " + playerMove + ". Could you try again?");            
                }
            }
            
        }


    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            
            String playerMove = getPlayerMove();
            String computerMove = getComputerMove(); 
            
            if (playerMove.equals(computerMove)) {
                System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". It's a tie!");
                }
            else if (playerMove.equals("rock")) {
                if (computerMove.equals("paper")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                    computerScore++;
                    }
                else if (computerMove.equals("scissors")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                    humanScore++;
                    }   
                }
            else if (playerMove.equals("paper")) {
                if (computerMove.equals("scissors")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                    computerScore++;
                    }
                else if (computerMove.equals("rock")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                    humanScore++;
                    }   
                }
            else if (playerMove.equals("scissors")) {
                if (computerMove.equals("rock")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                    computerScore++;
                    }   
                else if (computerMove.equals("paper")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                    humanScore++;
                    }
                }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        
            roundCounter++;

            System.out.println("Do you wish to continue playing? (y/n)? ");
            String playerChoice = sc.next();
            if (playerChoice.equals("n")) {
                break;
            }  
        }
        System.out.println("Bye bye :)");
    }

}

